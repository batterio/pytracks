__author__ = 'Loris Mularoni'


# Import modules
from setuptools import setup, find_packages
from pytracks import __version__


setup(
    name='pytracks',
    version=__version__,
    packages=find_packages(),
    package_data={'pytracks': ['templates/*.html', 'static/js/*', 'static/css/*', 'static/fonts/*']},
    include_package_data=True,
    url='http://www.lorismularoni.com',
    license='The MIT License (MIT)',
    author=__author__,
    author_email='loris.mularoni@gmail.com',
    description='',
    install_requires=['cherrypy', 'jinja2'],
    entry_points={
        'console_scripts': [
            'pytracks = pytracks.run:run'
        ]
    }
)
