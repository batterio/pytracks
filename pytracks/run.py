__author__ = 'Loris Mularoni'


# Import modules
import os
import cherrypy
from jinja2 import Environment, PackageLoader
from pytracks.webapp.app import WebApp
from pytracks.webapp.models import get_content
from .config import *
from cherrypy.lib import auth_digest


configuration = config['default']
USERS = {configuration.USERNAME: configuration.PASSWORD}


def run():
    """Starts the webapp"""
    # Create Jinja2 environment
    env = Environment(loader=PackageLoader('pytracks'))

    server_conf = {
        '/': {
            'tools.staticdir.root': os.path.dirname(os.path.abspath(__file__)),
            'tools.auth_digest.on': True,
            'tools.auth_digest.realm': 'localhost',
            'tools.auth_digest.get_ha1': auth_digest.get_ha1_dict_plain(USERS),
            'tools.auth_digest.key': 'a565c27146791cfb'
        },
        '/static': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': '/static'
        },
        #'/js': {
        #    'tools.staticdir.on': True,
        #    'tools.staticdir.dir': os.path.join('static', 'js')
        #},
        #'/css': {
        #    'tools.staticdir.on': True,
        #    'tools.staticdir.dir': os.path.join('static', 'css')
        #},
        #'/fonts': {
        #    'tools.staticdir.on': True,
        #    'tools.staticdir.dir': os.path.join('static', 'fonts')
        #}
    }

    print(server_conf)

    cherrypy.tree.mount(WebApp(env, get_content()), '/', server_conf)
    cherrypy.config.update({'server.socket_port': PORT})
    cherrypy.config.update({'server.socket_host': '0.0.0.0'})
    cherrypy.engine.start()
    cherrypy.engine.block()


if __name__ == "__main__":
    run()
