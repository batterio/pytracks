__author__ = 'Loris Mularoni'


# Import modules
import os
from collections import defaultdict
from pytracks.config import *


# Global variables
TOP_DIR = '.'
BIGWIG = ('.bw', '.bigwig')
conf = config['default']


def build_url(dir_name, file):
    """Define the url of the track"""
    if os.path.splitext(file)[1].lower() in BIGWIG:
        url = 'track type=bigWig name="{}" bigDataUrl=http://{}:'.format(file,
                                                                         conf.USERNAME)
        url += '{}@{}/authorisedAccess/tracksDirectory/{}/{}'.format(conf.PASSWORD,
                                                                     conf.URL,
                                                                     dir_name,
                                                                     file)
    else:
        url = 'http://{}:{}@{}/authorisedAccess/tracksDirectory/{}/{}'.format(conf.USERNAME,
                                                                              conf.PASSWORD,
                                                                              conf.URL,
                                                                              dir_name,
                                                                              file)
    return url


def get_content():
    """Return a dictionary with the content of the folders in data/"""
    d = defaultdict(dict)
    for path, dirs, files in os.walk(TOP_DIR):
        files = [f for f in files if not f.startswith('.')]

        if len(files) == 0:
            continue

        dir_name = os.path.split(path)[1]

        if dir_name.startswith("."):
            continue

        for file in files:
            d[dir_name][file] = build_url(dir_name, file)
    return d
