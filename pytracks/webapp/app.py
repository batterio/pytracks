__author__ = 'Loris Mularoni'


# Import modules
import cherrypy


class WebApp(object):
    """CherryPy app class"""
    def __init__(self, env, folders):
        self.env = env
        self.folders = folders

    @cherrypy.expose
    def index(self):
        """Intro page with the form"""
        return self.env.get_template('intro.html').render(data=self.folders)

    @cherrypy.expose
    def tracks(self, select_folder=None):
        """Tracks page"""
        if select_folder is None:
            raise cherrypy.HTTPRedirect("")

        return self.env.get_template('tracks.html').render(links=self.folders[select_folder])
