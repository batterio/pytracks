__author__ = 'Loris Mularoni'


PORT = 5000


class Config:
    USERNAME = 'username'
    PASSWORD = 'password'
    URL = 'something.org'


class Development(Config):
    DEBUG = True


class Production(Config):
    DEBUG = False


config = {'development': Development,
          'production': Production,
          'default': Development
          }
