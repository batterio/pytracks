# PyTracks 

This repository contains the code of PyTracks.  


## PyTracks dependencies  
* Python 3.4 (might work with previous python 3.x versions but it has not been tested)  
* Python 2.7 (might work with previous python 2.x versions but it has not been tested)  
* CherryPy  
* Jinja2  


## How to install PyTracks
PyTracks can be installed with two approaches:  

  
* ### Easy approach
    `pip install pytracks-0.1.0.tar.gz`  
    Then go inside your data/ folder and run `pytracks`
  
  
* ### Not that easy approach:  
    * Clone the PyTracks repository:  
        `git clone git@bitbucket.org:batterio/pytracks.git`  
    * Create a python virtual environment  
        * Install virtualenv and virtualenvwrapper  
            `pip install virtualenv`  
            `pip install virtualenvwrapper`  
        * Modify your bashrc (or bash_profile if you use OSX)  
            `export WORKON_HOME=$HOME/.virtualenvs`  
            `source /usr/local/bin/virtualenvwrapper.sh`  
        * Reset the terminal  
            `source ~/.bashrc` or in a Mac `source ~/.bash_profile`  
        * Create the virtual environment  
            `mkvirtualenv pytracks`  
    * Activate the newly created environment  
        `workon pytracks`  
    * Install the dependencies  
        `pip install -r requirements.txt`  
    * Run the server  
        `python run.py`  
    * Point your browser to:  
        `http://127.0.0.1:5000/`  
                

## TO DO  
* CSS with bootstrap  
